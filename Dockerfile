# This file is a template, and might need editing before it works on your project.
FROM chybie/node-aws-cli

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

# Install Nginx
RUN \
  apt-get install -y nginx && \
  chown -R www-data:www-data /var/lib/nginx

# Copy package.json for initial package installation
COPY . /usr/src/app/
# Update node Packages
RUN \
    npm install -g npm@3.10.7 && \
    npm install -g @angular/cli@6.0.8 && \
    npm install && \
    ng build

# Replace default nginx config
COPY conf/nginx.conf /etc/nginx/nginx.conf
# Remove default files from sites-enabled and sites-available
RUN rm -rf /etc/nginx/sites-enabled/default \
    rm -rf /etc/nginx/sites-available/default

# Replace this with your application's default port
EXPOSE 80 443

#
RUN chmod 777 /usr/src/app/startup.sh

# Run nginx
CMD /usr/src/app/startup.sh
